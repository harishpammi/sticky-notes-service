package com.noon.hackathon.bitsnbytes.dao;

import com.noon.commons.mysql.MysqlBaseDaoImpl;
import com.noon.commons.mysql.query.FindByQuery;
import com.noon.commons.mysql.query.InsertQuery;
import com.noon.commons.mysql.query.InsertQueryReturnId;
import com.noon.commons.mysql.query.UpdateQuery;
import com.noon.hackathon.bitsnbytes.dto.DashboardDTO;
import com.noon.hackathon.bitsnbytes.dto.PrivacyOptions;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;

/**
 * Created by hareesh.pammi on 2020-05-28
 */
public class DashboardDao extends MysqlBaseDaoImpl<Long, DashboardDTO> {
    public static final String INSERT_QUERY = "INSERT INTO `dashboards` (`user_id`, `name`, `privacy`) VALUES (?, ?, ?)";

    public static final String FIND_BY_ID_QUERY = "SELECT id, user_id, name, privacy FROM dashboards WHERE id=?";

    public static final String FIND_BY_USER_ID_QUERY = "SELECT id, user_id, name, privacy FROM dashboards WHERE user_id=?";

    public DashboardDao(JdbcTemplate jdbcTemplate, JdbcTemplate slaveJdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate, NamedParameterJdbcTemplate slaveNamedParameterJdbcTemplate) {
        super(jdbcTemplate, slaveJdbcTemplate, namedParameterJdbcTemplate, slaveNamedParameterJdbcTemplate);
    }

    protected InsertQuery getInsertQuery(DashboardDTO dashboardDTO) {
        PreparedStatementCallback<Boolean> statementCallback = statement -> {
            Integer index = 1;
            statement.setLong(index++, dashboardDTO.getUserId());
            statement.setString(index++, dashboardDTO.getName());
            statement.setString(index++, dashboardDTO.getPrivacy().getName());
            return statement.execute();
        };
        return new InsertQuery(INSERT_QUERY, statementCallback);
    }

    @Override
    protected UpdateQuery getUpdateQuery(DashboardDTO dashboardDTO) {
        return null;
    }

    @Override
    protected InsertQueryReturnId getInsertQueryReturnId(DashboardDTO dashboardDTO) {
        PreparedStatementCreator preparedStatementCreator = con -> {
            PreparedStatement statement = con.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            Integer index = 1;
            statement.setLong(index++, dashboardDTO.getUserId());
            statement.setString(index++, dashboardDTO.getName());
            statement.setString(index++, dashboardDTO.getPrivacy().getName());
            return statement;
        };

        return new InsertQueryReturnId(preparedStatementCreator);
    }

    @Override
    protected FindByQuery getFindByQuery(Long aLong) {
        return new FindByQuery(FIND_BY_ID_QUERY, new Object[]{aLong});
    }

    @Override
    protected FindByQuery getSelectAllQuery(Long aLong) {
        return new FindByQuery(FIND_BY_USER_ID_QUERY, new Object[]{aLong});
    }

    @Override
    protected RowMapper<DashboardDTO> getRowMapper() {
        return new RowMapper<DashboardDTO>() {
            @Override
            public DashboardDTO mapRow(ResultSet resultSet, int i) throws SQLException {
                return DashboardDTO.builder()
                        .id(resultSet.getLong("id"))
                        .userId(resultSet.getLong("user_id"))
                        .name(resultSet.getString("name"))
                        .privacy(PrivacyOptions.valueOf(resultSet.getString("privacy")))
                        .build();
            }
        };
    }
}
