package com.noon.hackathon.bitsnbytes.dao;

import com.noon.commons.mapper.MapperUtil;
import com.noon.commons.mysql.MysqlBaseDaoImpl;
import com.noon.commons.mysql.query.FindByQuery;
import com.noon.commons.mysql.query.InsertQuery;
import com.noon.commons.mysql.query.InsertQueryReturnId;
import com.noon.commons.mysql.query.UpdateQuery;
import com.noon.hackathon.bitsnbytes.dto.NotesDTO;
import com.noon.hackathon.bitsnbytes.dto.PrivacyOptions;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotesDao  extends MysqlBaseDaoImpl<Long, NotesDTO> {
    private static final String INSERT_QUERY = "INSERT INTO `notes` (`dashboard_id`, `name`, `privacy` , `content` , `metadata` , `type` , `is_active` ) VALUES (?, ?, ?,?,?,?,?)";

    private static final String FIND_BY_ID_QUERY = "SELECT id, `dashboard_id`, `name`, `privacy` , `content` , `metadata` , `type`  FROM notes WHERE id=?";

    private static final String FIND_BY_DASHBOARD_ID_QUERY = "SELECT id, `dashboard_id`, `name`, `privacy` , `content` , `metadata` , `type` FROM notes WHERE dashboard_id=? and is_active=1";

    public NotesDao(JdbcTemplate jdbcTemplate, JdbcTemplate slaveJdbcTemplate, NamedParameterJdbcTemplate namedParameterJdbcTemplate, NamedParameterJdbcTemplate slaveNamedParameterJdbcTemplate) {
        super(jdbcTemplate, slaveJdbcTemplate, namedParameterJdbcTemplate, slaveNamedParameterJdbcTemplate);
    }

    protected InsertQuery getInsertQuery(NotesDTO notesDTO) {
        PreparedStatementCallback<Boolean> statementCallback = statement -> {
            int index = 1;
            statement.setLong(index++, notesDTO.getDashboardId());
            statement.setString(index++, notesDTO.getName());
            statement.setString(index++, notesDTO.getPrivacy().getName());
            statement.setString(index++, notesDTO.getContent());
            statement.setString(index++, MapperUtil.convertObjectToString(notesDTO.getMetadata()));
            statement.setString(index++, notesDTO.getType());
            statement.setBoolean(index,notesDTO.getIsActive());
            return statement.execute();
        };
        return new InsertQuery<>(INSERT_QUERY, statementCallback);
    }

    @Override
    protected UpdateQuery getUpdateQuery(NotesDTO notesDTO) {


        return null;
    }

    @Override
    protected InsertQueryReturnId getInsertQueryReturnId(NotesDTO notesDTO) {
        PreparedStatementCreator preparedStatementCreator = con -> {
            PreparedStatement statement = con.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            int index = 1;
            statement.setLong(index++, notesDTO.getDashboardId());
            statement.setString(index++, notesDTO.getName());
            statement.setString(index++, notesDTO.getPrivacy().getName());
            statement.setString(index++, notesDTO.getContent());
            statement.setString(index++, MapperUtil.convertObjectToString(notesDTO.getMetadata()));
            statement.setString(index++, notesDTO.getType());
            statement.setBoolean(index, notesDTO.getIsActive());

            return statement;
        };

        return new InsertQueryReturnId(preparedStatementCreator);
    }

    @Override
    protected FindByQuery getFindByQuery(Long aLong) {
        return new FindByQuery(FIND_BY_ID_QUERY, new Object[]{aLong});
    }

    @Override
    protected FindByQuery getSelectAllQuery(Long aLong) {
        return new FindByQuery(FIND_BY_DASHBOARD_ID_QUERY, new Object[]{aLong});
    }

    public List<NotesDTO> findNotes(Long aLong) {
         return findAllBy(aLong);
    }

    public void update(NotesDTO notesDTO){

        String query = "UPDATE notes SET ";
        Map<String, Object> params = new HashMap<>();

        if (notesDTO.getName() != null) {
            query = query + " name = :name,";
            params.put("name", notesDTO.getName());
        }

        if (notesDTO.getType() != null) {
            query = query + " type = :groupType,";
            params.put("groupType", notesDTO.getType());
        }

        if (notesDTO.getContent() != null) {
            query = query + " content = :getContent,";
            params.put("getContent", notesDTO.getContent());
        }

        if (notesDTO.getMetadata() != null) {
            query = query + " metadata = :metadata,";
            params.put("metadata", MapperUtil.convertObjectToString(notesDTO.getMetadata()));
        }

        if(notesDTO.getIsActive() != null){
            query = query + " is_active = :isActive,";
            params.put("isActive", notesDTO.getIsActive());
        }

        query =query.substring(0,query.lastIndexOf(","));

        query = query + " where id = :noteId and dashboard_id = :dashboardId" ;
        params.put("noteId", notesDTO.getId());
        params.put("dashboardId", notesDTO.getDashboardId());

        namedParameterJdbcTemplate.update(query, params);
    }

    @Override
    protected RowMapper<NotesDTO> getRowMapper() {
        return new RowMapper<NotesDTO>() {
            @Override
            public NotesDTO mapRow(ResultSet resultSet, int i) throws SQLException {
                return NotesDTO.builder()
                        .id(resultSet.getLong("id"))
                        .dashboardId(resultSet.getLong("dashboard_id"))
                        .name(resultSet.getString("name"))
                        .privacy(PrivacyOptions.valueOf(resultSet.getString("privacy")))
                        .content(resultSet.getString("content"))
                        .metadata(MapperUtil.convertStringToMap(resultSet.getString("metadata")))
                        .type(resultSet.getString("type"))
                        .build();
            }
        };
    }
}