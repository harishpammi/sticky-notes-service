package com.noon.hackathon.bitsnbytes.config;

import com.noon.commons.config.MysqlConfig;
import com.noon.commons.mysql.MysqlJDBCConnectionPool;
import com.noon.commons.mysql.MysqlJDBCTemplate;
import com.noon.hackathon.bitsnbytes.dao.DashboardDao;
import com.noon.hackathon.bitsnbytes.dao.NotesDao;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by hareesh.pammi on 2020-05-28
 */
@Data
@Configuration
@ConfigurationProperties("sticky-notes-config")
public class StickyNotesConfig {
    private MysqlConfig mysqlConfig;

    @Bean
    public MysqlJDBCConnectionPool getMysqlJDBCConnectionPool() {
        return new MysqlJDBCConnectionPool(new MysqlJDBCTemplate(mysqlConfig), new MysqlJDBCTemplate(mysqlConfig));
    }

    @Bean
    public DashboardDao getDashboardDao(MysqlJDBCConnectionPool mysqlJDBCConnectionPool) {
        return new DashboardDao(mysqlJDBCConnectionPool.getMasterMysqlJDBCTemplate().getJdbcTemplate(), mysqlJDBCConnectionPool.getMasterMysqlJDBCTemplate().getJdbcTemplate(), mysqlJDBCConnectionPool.getMasterMysqlJDBCTemplate().getNamedParameterJdbcTemplate(), mysqlJDBCConnectionPool.getMasterMysqlJDBCTemplate().getNamedParameterJdbcTemplate());
    }

    @Bean
    public NotesDao getNotesDao(MysqlJDBCConnectionPool mysqlJDBCConnectionPool) {
        return new NotesDao(mysqlJDBCConnectionPool.getMasterMysqlJDBCTemplate().getJdbcTemplate(), mysqlJDBCConnectionPool.getMasterMysqlJDBCTemplate().getJdbcTemplate(), mysqlJDBCConnectionPool.getMasterMysqlJDBCTemplate().getNamedParameterJdbcTemplate(), mysqlJDBCConnectionPool.getMasterMysqlJDBCTemplate().getNamedParameterJdbcTemplate());
    }
}
