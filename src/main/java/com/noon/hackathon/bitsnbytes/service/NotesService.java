package com.noon.hackathon.bitsnbytes.service;

import com.noon.hackathon.bitsnbytes.dao.NotesDao;
import com.noon.hackathon.bitsnbytes.dto.NotesDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotesService {

    @Autowired
    private NotesDao notesDao;

    public NotesDTO createNote(NotesDTO notesDTO) {
        Long id = notesDao.saveWithReturnId(notesDTO);
        notesDTO.setId(id);
        return notesDTO;
    }

    public void update(NotesDTO notesDTO) {
        notesDao.update(notesDTO);
    }

    public List<NotesDTO> getNotes(Long dashboardId) {
        return notesDao.findAllBy(dashboardId);
    }

    public void delete(NotesDTO notesDTO) {
        notesDTO.setIsActive(false);
        notesDao.update(notesDTO);

    }
}
