package com.noon.hackathon.bitsnbytes.service;

import com.noon.hackathon.bitsnbytes.dao.DashboardDao;
import com.noon.hackathon.bitsnbytes.dto.DashboardDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hareesh.pammi on 2020-05-28
 */
@Service
public class DashboardService {
    @Autowired
    private DashboardDao dashboardDao;

    public DashboardDTO createDashboard(DashboardDTO dashboardDTO) {
        Long aLong = dashboardDao.saveWithReturnId(dashboardDTO);
        dashboardDTO.setId(aLong);

        return dashboardDTO;
    }

    public List<DashboardDTO> getUserDashboards(Long userId) {
        List<DashboardDTO> allByMaster = dashboardDao.findAllByMaster(userId);

        return allByMaster;
    }
}
