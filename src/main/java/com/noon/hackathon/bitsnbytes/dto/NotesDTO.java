package com.noon.hackathon.bitsnbytes.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotesDTO {

    private Long id;

    private Long dashboardId;

    private String content;

    private String name;

    private PrivacyOptions privacy;

    private String type;

    private Map<String,String> metadata;

    @JsonIgnore
    private Boolean isActive;


}
