package com.noon.hackathon.bitsnbytes.dto;

import lombok.Getter;

/**
 * Created by hareesh.pammi on 2020-05-28
 */
@Getter
public enum PrivacyOptions {
    PUBLIC("PUBLIC"), PRIVATE("PRIVATE");

    private String name;

    PrivacyOptions(String name) {
        this.name = name;
    }
}
