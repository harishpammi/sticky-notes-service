package com.noon.hackathon.bitsnbytes.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * Created by hareesh.pammi on 2020-05-28
 */
@Data
public class DashboardDTO {
    private Long id;

    private Long userId;

    private String name;

    private PrivacyOptions privacy;

    private List<NotesDTO> notes;

    @Builder
    public DashboardDTO(Long id, Long userId, String name, PrivacyOptions privacy, List<NotesDTO> notes) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.privacy = privacy;
        this.notes = notes;
    }
}
