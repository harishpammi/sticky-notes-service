package com.noon.hackathon.bitsnbytes.resource;

import com.noon.hackathon.bitsnbytes.dto.NotesDTO;
import com.noon.hackathon.bitsnbytes.service.NotesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/sticky-notes-service/v1/notes")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class NotesResource {

    @Autowired
    private NotesService notesService;

    @PostMapping("/{dashboard_id}")
    public ResponseEntity createNote(@PathVariable(name = "dashboard_id", required = true) Long dashboardId,@RequestBody NotesDTO notesDTO) {
        notesDTO.setDashboardId(dashboardId);
        notesDTO.setIsActive(true);
        return ResponseEntity.status(HttpStatus.CREATED).body( notesService.createNote(notesDTO));
    }

    @PutMapping("/{dashboard_id}/{note_id}")
    public ResponseEntity updateNote(@PathVariable(name = "dashboard_id", required = true) Long dashboardId,@PathVariable(name = "note_id", required = true) Long noteId,@RequestBody NotesDTO notesDTO) {
        notesDTO.setDashboardId(dashboardId);
        notesDTO.setId(noteId);
        notesDTO.setIsActive(true);
        notesService.update(notesDTO);

        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    @DeleteMapping("/{dashboard_id}/{note_id}")
    public ResponseEntity deleteNote(@PathVariable(name = "dashboard_id", required = true) Long dashboardId,@PathVariable(name = "note_id", required = true) Long noteId) {
        NotesDTO notesDTO = NotesDTO.builder().isActive(false).build();

        notesDTO.setDashboardId(dashboardId);
        notesDTO.setId(noteId);
        notesService.delete(notesDTO);

        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    @GetMapping(value = "/{dashboard_id}")
    public ResponseEntity getAllNotes(@RequestParam(name = "user_id", required = true) Long userId, @PathVariable(name = "dashboard_id", required = true) Long dashboardId) {
        List<NotesDTO> userDashboards = notesService.getNotes(dashboardId);
        return ResponseEntity.status(HttpStatus.OK).body(userDashboards);
    }
}
