package com.noon.hackathon.bitsnbytes.resource;

import com.noon.hackathon.bitsnbytes.dto.DashboardDTO;
import com.noon.hackathon.bitsnbytes.service.DashboardService;
import com.noon.hackathon.bitsnbytes.service.NotesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by hareesh.pammi on 2020-05-28
 */
@Slf4j
@RestController
@RequestMapping("/sticky-notes-service/v1/dashboards")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DashboardResource {
    @Autowired
    private DashboardService dashboardService;

    @Autowired
    private NotesService notesService;

    @PostMapping
    public ResponseEntity createDashboard(@RequestBody DashboardDTO dashboardDTO) {
        DashboardDTO dashboard = dashboardService.createDashboard(dashboardDTO);

        return ResponseEntity.status(HttpStatus.CREATED).body(dashboard);
    }

    @GetMapping(value = "/all")
    public ResponseEntity getAllUserDashboard(@RequestParam(name = "user_id", required = true) Long userId) {
        List<DashboardDTO> userDashboards = dashboardService.getUserDashboards(userId);
        userDashboards.forEach(dashboardDTO -> {
            dashboardDTO.setNotes(notesService.getNotes(dashboardDTO.getId()));
        });

        return ResponseEntity.status(HttpStatus.OK).body(userDashboards);
    }
}
