#!/bin/bash

java -XX:+UseG1GC -Xmx2g -Xms256m -XX:NewRatio=2 -XX:+DisableExplicitGC -XX:+UseTLAB -XX:-UseBiasedLocking -XX:MetaspaceSize=256m -XX:MaxMetaspaceSize=256m -XX:+AggressiveOpts -XX:+UseCompressedOops -jar /app/sticky-notes.jar --spring.config.location=/app/sticky-notes.yml
