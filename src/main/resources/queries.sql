CREATE DATABASE `sticky_notes`;

USE `sticky_notes`;

CREATE TABLE `dashboards` (
  `id` bigint NOT NULL auto_increment,
  `user_id` bigint NOT NULL,
  `name` varchar(100) NOT NULL,
  `privacy` varchar(20) NOT NULL,
  `deleted` boolean DEFAULT false,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) default null,
  `updated_by` varchar(50) default null,

  PRIMARY KEY (`id`),
  INDEX `idx_user_id` (`user_id`) COMMENT 'Index on user_id column',
  INDEX `idx_deleted` (`deleted`) COMMENT 'Index on deleted column'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `notes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dashboard_id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `privacy` varchar(20) NOT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `type` varchar(20) NOT NULL,
  `content` varchar(2000) DEFAULT NULL,
  `metadata` json DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_dashboard_id` (`dashboard_id`) COMMENT 'Index on dashboard_id column'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;