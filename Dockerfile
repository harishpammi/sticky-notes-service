FROM maven:3.5.2-jdk-8 AS build
MAINTAINER Hareesh Pammi <hareesh@noonacademy.com>

ADD settings.xml /root/.m2/settings.xml
COPY src /app/src
COPY pom.xml /app
COPY src/main/resources/run.sh /app
RUN ls /app/
RUN mvn -f /app/pom.xml clean install

FROM openjdk:8-jdk-alpine
RUN apk add --no-cache bash
WORKDIR /app
COPY --from=build /app/target/sticky-notes-*.jar /app/sticky-notes.jar
COPY --from=build /app/target/classes/sticky-notes.yml /app
COPY --from=build /app/run.sh /app

RUN ls /app/
RUN chmod +x /app/run.sh
#ENV TZ=Asia/Calcutta

ENTRYPOINT ["/app/run.sh"]